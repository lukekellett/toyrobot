module.exports = exports = {
    welcomeMessage: 'Toy Robot Simulator',
    validCommands: `Valid Commands:
    place   Place the robot anywhere on the table (parameters: x, y, f)
    move    Move the robot forward in the currently faced direction
    left    Turn the robot left
    right   Turn the robot right
    report  Report the robots current position and direction faced
    help    Show this command list
    quit    Quit the program
    `,
    commandPromptPrefix: '> ',
    currentPosition: 'Current position is x: %s, y: %s, facing %s.',
    error: {
        invalidCommand: 'Invalid command "%s".',
        positionNotSet: 'Position of the robot has not been set, use the "place" command first.',    
        positionParametersRequried: 'X, Y and F position parameters requried! e.g. "place 1,2,n".',
        positionParametersInvalid: 'Position parameters "%s" are invalid. Expected "x,y,f"',
        invalidNumber: 'Invalid number "%s"',
        invalidDirection: 'Invalid direction to face "%s". Expected "n", "e", "s" or "w".',
        outOfRange: '"%s" is out of range in the %s dimension. Expected a value between 0-%s.',
        outOfRangeMove: 'I\'m sorry Dave, I can\'t do that. It would place your robot outside of the table.',
    },
    directions: {
        n: 'north',
        e: 'east',
        s: 'south',
        w: 'west',
    }
}
var util = require('util')
var constants = require('./constants')
var strings = require('./strings.js')
var sprintf = require('sprintf-js').sprintf
var stdin = process.openStdin()
var stdout = process.stdout

var position = null // {x: number, y: number, f: [n,e,s,w]}
module.exports.position = position

// Variable so it can be injected for testing
var writeOutput = function(output, lineFeeds) {
    process.stdout.write(output)

    if(util.isNumber(lineFeeds)) {
        for(var i = 0; i < lineFeeds; i++) {
            process.stdout.write('\n')
        }
    }
}

module.exports.main = function() {
    console.log(strings.welcomeMessage, 1)
    console.log(strings.validCommands)
    process.stdout.write(strings.commandPromptPrefix)
    
    stdin.addListener('data', d => {
        // Stript the linefeed from the input
        let command = d.toString().trim().toLowerCase()
        
        if(!parseCommand(command)) {
            writeOutput(sprintf(strings.error.invalidCommand, command), 2)
            writeOutput(strings.validCommands, 1)
        }

        writeOutput(strings.commandPromptPrefix)
    
    })

}

var parseCommand = function(command) {

    var commandWithoutParameters = command

    if(command.indexOf(' ') > -1) {
        commandWithoutParameters = command.substring(0, command.indexOf(' '))
    }

    switch(commandWithoutParameters) {
    case constants.command.place:
        place(command)
        break;

    case constants.command.move:
        move()
        break;

    case constants.command.report:
        report()
        break;

    case constants.command.turnLeft:
        turnLeft()
        break;
        
    case constants.command.turnRight:
        turnRight()
        break;
        
    case constants.command.quit:
        process.exit(-1)
        break;

    case constants.command.help:
        help()
        break;

    default:
        return false
    }

    return true

}

function validateCurrentPosition() {
    if(util.isNull(position)) {
        writeOutput(strings.error.positionNotSet, 2)
        return false
    } else {
        return true
    }
}

function isNumeric(value){
    return !isNaN(value)
}

function place(command) {
    // Ensure that parameters were specified
    let parametersStart = command.indexOf(' ')
    if(parametersStart == -1) {
        writeOutput(strings.error.positionParametersRequried, 2)
        return
    }
    
    let commandParameters = command.substring(parametersStart + 1)
    let commandParametersSplit = commandParameters.split(',')

    if(commandParametersSplit.length != 3) {
        writeOutput(sprintf(strings.error.positionParametersInvalid, commandParameters), 2)
        return
    }

    // Do a little clean up
    for(var i = 0; i < 3; i++) {
        commandParametersSplit[i] = commandParametersSplit[i].trim()
    }

    // Parse into usable variables
    var x = 0
    var y = 0
    var f = 0

    if(isNumeric(commandParametersSplit[0])) {
        x = parseInt(commandParametersSplit[0])

        if(x < 0 || x > constants.tableSize.x - 1) {
            writeOutput(sprintf(strings.error.outOfRange, x, 'x', constants.tableSize.x - 1), 2)
            return
        }

    } else {
        writeOutput(sprintf(strings.error.invalidNumber, commandParametersSplit[0]), 2)
        return
    }
    
    if(isNumeric(commandParametersSplit[1])) {
        y = parseInt(commandParametersSplit[1])
        if(y < 0 || y > constants.tableSize.y - 1) {
            writeOutput(sprintf(strings.error.outOfRange, y, 'y', constants.tableSize.y - 1), 2)
            return
        }

    } else {
        writeOutput(sprintf(strings.error.invalidNumber, commandParametersSplit[1]), 2)
        return
    }
    
    if(constants.validDirections.indexOf(commandParametersSplit[2]) > -1) {
        f = commandParametersSplit[2]

        if(f.length > 1) {
            f = f.substring(0, 1)
        }

    } else {
        writeOutput(sprintf(strings.error.invalidDirection, commandParametersSplit[2]), 2)
        return
    }
    
    position = {
        x: x,
        y: y,
        f: f,
    }

    report()

}

function move() {
    if(!validateCurrentPosition()) {
        return
    }

    // Determine what dimension we are to move on
    let moveX = position.f == 'e' || position.f == 'w'
    let moveBy = position.f == 'n' || position.f == 'e' ? 1 : -1

    if(moveX) {
        let newX = position.x + moveBy

        if(newX < 0 || newX > constants.tableSize.x - 1) {
            writeOutput(strings.error.outOfRangeMove, 2)
            return
        }

        position.x = newX

    } else {
        let newY = position.y + moveBy

        if(newY < 0 || newY > constants.tableSize.y - 1) {
            writeOutput(strings.error.outOfRangeMove, 2)
            return
        }

        position.y = newY

    }

    report()

}

function report() {
    if(!validateCurrentPosition()) {
        return
    }

    writeOutput(sprintf(strings.currentPosition, position.x, position.y, strings.directions[position.f]), 2)

}

function turnRight() {
    if(!validateCurrentPosition()) {
        return
    }
    var directionIndex = constants.validDirections.indexOf(position.f)
    
    if(directionIndex == constants.validDirections.length - 1) {
        directionIndex = 0
    } else {
        directionIndex += 1
    }
    
    position.f = constants.validDirections[directionIndex]

    report()

}

function turnLeft() {
    if(!validateCurrentPosition()) {
        return
    }

    var directionIndex = constants.validDirections.indexOf(position.f)
    
    if(directionIndex == 0) {
        directionIndex = constants.validDirections.length - 1
    } else {
        directionIndex -= 1
    }
    
    position.f = constants.validDirections[directionIndex]

    report()

}

function help() {
    writeOutput(strings.validCommands, 1)
}

module.exports.setWriteOutput = function(newWriteOutput) {
    writeOutput = newWriteOutput
}

module.exports.clearPosition = function() {
    position = null
}

module.exports.setPosition = function(x, y, f) {
    position = {
        x: x,
        y: y,
        f: f,
    }
}

module.exports.parseCommand = parseCommand
module.exports = exports = {
    tableSize: {
        x: 5,
        y: 5,
    },
    command: {
        place: 'place',
        move: 'move',
        turnLeft: 'left',
        turnRight: 'right',
        report: 'report',
        quit: 'quit',
        help: 'help',
    },
    direction: {
        north: 'n',
        east: 'e',
        south: 's',
        west: 'w',
    },
    validDirections: ['n', 'e', 's', 'w', 'north', 'east', 'south', 'west'],
}
var assert = require('assert')
var app = require('../app/app')

var lastLineFeeds = null
var lastOutput = null

app.setWriteOutput(function(output, lineFeeds) {
    lastLineFeeds = lineFeeds
    lastOutput = output
})

function clearOutput() {
    lastLineFeeds = null
    lastOutput = null
}

function clearPosition() {
    app.clearPosition()
}

function setup() {
    clearOutput()
    clearPosition()
}

describe('App', function() {
    describe('parseCommand(place)', function() {
        it('should fail when no arguments passed', function() {
            setup()
            app.parseCommand("place")
            assert.equal(lastOutput, 'X, Y and F position parameters requried! e.g. "place 1,2,n".')
        })         
        it('should fail when invalid number of arguments passed', function() {
            setup()
            app.parseCommand("place 1,2")
            assert.equal(lastOutput, 'Position parameters "1,2" are invalid. Expected "x,y,f"')
        })        
        it('should fail when X not a number', function() {
            setup()
            app.parseCommand("place x,1,n")
            assert.equal(lastOutput, 'Invalid number "x"')
        })
        it('should fail when Y not a number', function() {
            setup()
            app.parseCommand("place 1,y,n")
            assert.equal(lastOutput, 'Invalid number "y"')
        })
        it('should fail when F is not a direction', function() {
            setup()
            app.parseCommand("place 1,1,a")
            assert.equal(lastOutput, 'Invalid direction to face "a". Expected "n", "e", "s" or "w".')
        })   
        it('should fail when X is out of range', function() {
            setup()
            app.parseCommand("place -1,0,n")
            assert.equal(lastOutput, '"-1" is out of range in the x dimension. Expected a value between 0-4.')
        })   
        it('should fail when X is out of range', function() {
            setup()
            app.parseCommand("place 5,0,n")
            assert.equal(lastOutput, '"5" is out of range in the x dimension. Expected a value between 0-4.')
        })   
        it('should fail when Y is out of range', function() {
            setup()
            app.parseCommand("place 0,-1,n")
            assert.equal(lastOutput, '"-1" is out of range in the y dimension. Expected a value between 0-4.')
        })   
        it('should fail when Y is out of range', function() {
            setup()
            app.parseCommand("place 0,5,n")
            assert.equal(lastOutput, '"5" is out of range in the y dimension. Expected a value between 0-4.')
        })
        it('should pass when X, Y and F are valid', function() {
            setup()
            app.parseCommand("place 1,4,e")
            assert.equal(lastOutput, 'Current position is x: 1, y: 4, facing east.')
        })  
    })
    describe('parseCommand(report)', function() {
        it('should fail when position not set', function() {
            setup()
            app.parseCommand("report")
            assert.equal(lastOutput, 'Position of the robot has not been set, use the "place" command first.')
        })
        it('should pass when position not set', function() {
            setup()
            app.setPosition(0, 2, 'e')
            app.parseCommand("report")
            assert.equal(lastOutput, 'Current position is x: 0, y: 2, facing east.')
        })
    })
    describe('parseCommand(left)', function() {
        it('should fail when position not set', function() {
            setup()
            app.parseCommand("move")
            assert.equal(lastOutput, 'Position of the robot has not been set, use the "place" command first.')
        })
        it('should rotate 90 degrees CCW', function() {
            setup()
            app.setPosition(0,0,'n')
            app.parseCommand("left")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing west.')
            app.parseCommand("left")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing south.')
            app.parseCommand("left")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing east.')
            app.parseCommand("left")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing north.')
        })
    })
    describe('parseCommand(right)', function() {
        it('should fail when position not set', function() {
            setup()
            app.parseCommand("right")
            assert.equal(lastOutput, 'Position of the robot has not been set, use the "place" command first.')
        })
        it('should rotate 90 degrees CW', function() {
            setup()
            app.setPosition(0,0,'n')
            app.parseCommand("right")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing east.')
            app.parseCommand("right")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing south.')
            app.parseCommand("right")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing west.')
            app.parseCommand("right")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing north.')
        })
    })
    describe('parseCommand(move)', function() {
        it('should fail when position not set', function() {
            setup()
            app.parseCommand("move")
            assert.equal(lastOutput, 'Position of the robot has not been set, use the "place" command first.')
        })
        it('should move north to edge of table', function() {
            setup()
            app.setPosition(0,0,'n')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 1, facing north.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 2, facing north.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 3, facing north.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 4, facing north.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'I\'m sorry Dave, I can\'t do that. It would place your robot outside of the table.')            
        })
        it('should move east to edge of table', function() {
            setup()
            app.setPosition(0,0,'e')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 1, y: 0, facing east.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 2, y: 0, facing east.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 3, y: 0, facing east.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 4, y: 0, facing east.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'I\'m sorry Dave, I can\'t do that. It would place your robot outside of the table.')            
        })
        it('should move south to edge of table', function() {
            setup()
            app.setPosition(0,4,'s')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 3, facing south.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 2, facing south.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 1, facing south.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing south.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'I\'m sorry Dave, I can\'t do that. It would place your robot outside of the table.')            
        })
        it('should move west to edge of table', function() {
            setup()
            app.setPosition(4,0,'w')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 3, y: 0, facing west.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 2, y: 0, facing west.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 1, y: 0, facing west.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'Current position is x: 0, y: 0, facing west.')
            app.parseCommand("move")
            assert.equal(lastOutput, 'I\'m sorry Dave, I can\'t do that. It would place your robot outside of the table.')            
        })
    })
})
# README #

Developed on OSX using Visual Studio Code.

### Setup ###

* Install node.js and npm.
* Pull the repo down and open a terminal to the repo location.
* Execute 'node index.js' to launch the app.
* Execute 'npm test' to run tests.
